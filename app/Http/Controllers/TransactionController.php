<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Transaction;
use App\Models\Account;
use App\Models\CurrencyExchange;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    public function show(Request $request){
        $user = auth()->user();
        $query = DB::table('transactions as t');
        $query->join('accounts as a', 'a.id', '=', 't.account_from_id');
        $query->where('a.user_id' , $user->id);
        if($request->input("From")){
            $query->where('t.created_at' , '>=', $request->input("From"));
        }
        if($request->input("To")){
            $query->where('t.created_at' , '<=', $request->input("To"));
        }
        if($request->input("SourceAccountID")){
            $query->where('a.account_id' , $request->input("SourceAccountID"));
        }

        $result = $query->get();
        return json_encode($result);

    }

    public function create(Request $request){
        if ($request->has(['accountFrom', 'accountTo', 'amount'])) {
            $accountFrom = $request->input('accountFrom');
            $accountTo = $request->input('accountTo');
            $amount = (float) $request->input('amount');

            $user = User::find(1);
            $accounts = $user->accounts;

            $accountFrom_m = Account::where('account_id', $accountFrom)->get()[0];
            $accountTo_m = Account::where('account_id', $accountTo)->get()[0];

            $transaction = new Transaction;
            $transaction->account_from_id = $accountFrom_m->id;
            $transaction->account_to_id = $accountTo_m->id;
            $transaction->amount = $amount;

            $comision = 0;
            if($accountFrom_m->user_id!=$accountTo_m->user_id){
                //distintos usuarios entre cuentas
                $comision = (float) $amount/100;
            }

            if($accountFrom_m->currency_exchange_id == $accountTo_m->currency_exchange_id){
                //misma moneda
                $accountFrom_m->balance = $accountFrom_m->balance - $amount - $comision;
                $accountTo_m->balance = $accountTo_m->balance + $amount;

            }else{
                //cambio de divisa
                $accountFrom_m->balance = $accountFrom_m->balance - $amount - $comision;
                $this->currency_exchange_query();
                $currency_exchange_from_m = CurrencyExchange::find($accountFrom_m->currency_exchange_id);
                $currency_exchange_to_m = CurrencyExchange::find($accountTo_m->currency_exchange_id);

                $exchange_amount = (float) ( (float) $amount / $currency_exchange_from_m->value ) * $currency_exchange_to_m->value;
                $accountTo_m->balance = $accountTo_m->balance + $exchange_amount;
            }
            
            $accountFrom_m->save();
            $accountTo_m->save();
            $transaction->save();
            return "transacción realizada";
        }else{
            return "faltan parametros";
        };

        
    }

    public function currency_exchange_query(){
        //esta función la implementé acá pero yo sé que no va en el Controller, en Symfony usabamos repositorios para este tipo de funciones, pero no encontré como hacer algo similar en laravel
        $now = now();
        $all_currency_exchange = CurrencyExchange::all();
        $currency_exchange_date = $all_currency_exchange[0]->updated_at;

        $dDiff = $now->diff($currency_exchange_date)->format('%d');
        if($dDiff>=1){
            //paso un día, actualizo divisas
            $response = Http::get('http://data.fixer.io/api/latest?access_key=49ca844a5d6ff6652167e370a2716eda');
            $json_response = json_decode($response, true);
            $rates = $json_response["rates"];
            foreach($all_currency_exchange as $ce){
                print($ce->name);
                $ce->value = $rates[$ce->name];
                $ce->updated_at = $now;
                $ce->save();
            }

        }
    }
}
