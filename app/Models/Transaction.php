<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    //relacion n:1 Account from
    public function account_from(){
        return $this->belongsTo("App\Models\Account");
    }

    //relacion n:1 Account to
    public function account_to(){
        return $this->belongsTo("App\Models\Account");
    }
}
