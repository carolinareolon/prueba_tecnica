<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CurrencyExchange extends Model
{
        //relacion 1:n Account
        public function accounts(){
            return $this->hasMany("App\Models\Account");
        }
}
