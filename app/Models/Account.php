<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Account extends Model
{
        //relacion n:1 User
        public function user(){
            return $this->belongsTo("App\Models\User");
        }
}
