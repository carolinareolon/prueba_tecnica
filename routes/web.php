<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransactionController;
use App\Http\Controllers\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/login', [LoginController::class, 'authenticate']);

//Route::middleware('auth:api')->get('/transactions', [TransactionController::class, 'show']);
Route::get('/transactions', [TransactionController::class, 'show']);

//Route::middleware('auth:api')->post('/transfer', [TransactionController::class, 'create']);
Route::post('/transfer', [TransactionController::class, 'create']);
